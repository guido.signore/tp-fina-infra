
FROM ubuntu:latest
LABEL version="1.5"
LABEL description="Cotizacion Crypto"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install nginx -y

ADD https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/index.html /var/www/html/
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/default /etc/nginx/sites-available/

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
