# TP Final

Fecha: 7/12/2021

Alumno: Guido Signore

Materia: Infraestructura de servidores

Profesor: Sergio Pernas

## Link al repositorio:

https://gitlab.com/guido.signore/tp-fina-infra.git

## Proyecto

Armar un contenedor que pueda ser montado rápidamente mediante un Dockerfile para mostrar una pagina web con cotizaciones de 3 criptomonedas (Bitcoin, SHIBA inu, Ethereum) y se actualice cada 10 segundos la cotización, montado sobre un servidor nginx.

### Archivo Dockerfile:

```
#Primero se especifica la imagen base para el contenedor, en este caso Ubuntu en su ultima version
FROM ubuntu:latest

#Con el comando LABEL se da mas informacion sobre el proyecto, en este caso el numero de version
#y una descripcion
LABEL version="1.5"
LABEL description="Cotizacion Crypto"

#Con el comando ARG le enviamos un argumento al momento de crear la imagen. En este caso ya que la idea
#es automatizar, lo que le indicamos en esta linea es que no se va a intervenir al momento de hacer el deploy
#de la imagen base asi no se realiza ninguna pregunta y se hace una instalacion default
ARG DEBIAN_FRONTEND=noninteractive

#Ya que en este proyecto vamos a lanzar una pagina web que indiquen unos datos, estos se montan sobre un servidor
#nginx, con la siguiente linea ejecutamos la instalación con la opcion "-y" para que si se pregunta
#en algun momento si se desea continuar que se responda "Si" automaticamente
RUN apt update && apt install nginx -y

#Con el comando ADD descarcamos los archivos de nuestro git y los ponemos en el directorio que deseamos.
#Primero se pone el archivo index.html que contiene la pagina que vamos a mostrar en el directorio root
#de nginx
ADD https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/index.html /var/www/html/
#Asignamos permisos al directorio root para que se pueda mostrar la pagina correctamente y no nos
#muestre el error "forbbiden"
RUN chmod +r /var/www/* -R
#Descargamos el archivo "default" con las configuraciones de nginx
ADD https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/default /etc/nginx/sites-available/

#Habilitamos los puertos 80(http) y 443(htttps) para que haya comunicacion
EXPOSE 80 443

#Con el comando CMD ejecutamos comandos directamente. En este caso especificamos que nginx
#funcione siempre en primer plano. Esto es recomendable cuando se usan contenedores ya que va a ser
#la unica instancia de nginx que va a haber. Si se quiere otra instancia se lanza otro contenedor.
CMD ["nginx", "-g", "daemon off;"]
```

### Archivo Default

```
server {
        listen 80 default_server; #Le decimos que el servidor se ejecute en el puerto 80
        root /var/www/html; #Informamos cual es la carpeta default donde estan los archivos de la web
        index index.html index.htm index.nginx-debian.html; #definimos los nombres por default de los archivos base
        server_name _; #especificamos si se quiere un nombre de servidor
        location / {
                try_files $uri $uri/ =404; #el error que va a mostrar en caso de no encontrar un archivo para mostrar
        }
}
```

## Configuraciones preliminares

### Instalacion de docker

Primero se instalan las dependencias y paquetes necesarios

apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common

Luego se instala la llave

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

Seguimos agregando el repositorio de docker

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

Ejecutamos un apt update para asegurarnos que este todo actualizado

Luego se instala docker

apt install docker-ce docker-ce-cli containerd.io

Para finalizar se instala docker compose

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

y se asignan los permisos necesarios

sudo chmod +x /usr/local/bin/docker-compose

### Clonacion de repositorio

Lo primero que hay que hacer es conectar la pc (en nuestro caso una maquina virtual) con nuestro GIT. Para ello, hay que generar una clave publica para encriptar los datos que se vayan a comunicar. Esto lo realizamos con el siguiente comando "ssh-keygen" con las variables "-t", el tipo de encriptacion y un comentario si se desea; vamos a usar rsa2048, por lo que el comando completo queda:

ssh-keygen -t rsa -b 2048 -C "Servidor TP Final Infra"

Luego nos preguntara en donde se guarda la clave (podemos dejarlo por default en el directorio /root/.ssh/) si queremos poner una contraseña para la comunicación. Siempre es recomendable para mas protección asignar una pero como estamos en un entorno de prueba y sin información sensible para simplificar pasos no colocamos ninguna. Una vez generada la clave, vamos al directorio donde se guardo y copiamos el contenido del archivo id_rsa.pub que es nuestra clave publica para compartir. Esa clave la agregamos en las configuraciones de nuestro git.

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/key.png)

Lo que sigue es clonar el repositorio con el comando "git clone (se coloca la dirección web del repositorio)" en mi caso:

git clone https://gitlab.com/guido.signore/tp-fina-infra

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/clone.png)

Ya con estos pasos tenemos el archivo Dockerfile para poder construir la imagen.

### Construir imagen y lanzar contenedor

Al clonar el repositorio nos va a crear un directorio con el mismo nombre donde tiene que estar el archivo Dockerfile. Nos posicionamos en ese directorio y ejecutamos el comando "docker build" con la opcion -t para asignar un nombre:

docker build -t Informacion-crypto .

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/build.png)

podemos chequear que la imagen fue creada correctamente con el comando "docker images"

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/images.png)

Luego de crear la imagen lo que sigue es ejecutar esa imagen para crear el contenedor con el comando "docker run" con las opciones "-d"(ejecutarse en modo daemon) "-p" (para asignar el puerto) "--name" (para darle nombre al contenedor) y el nombre de la imagen

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/run.png)

Solo queda probarlo ingresando a la ip del servidor apuntando al puerto asignado, en este caso el 81

Podemos chequear que el contenedor esta ejecutándose con el comando docker ps

![](https://gitlab.com/guido.signore/tp-fina-infra/-/raw/main/Imagenes/ps.png)